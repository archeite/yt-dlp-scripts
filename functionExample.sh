#!/bin/bash
YTSCRIPTDIR="$HOME/yt-dlp-scripts/scripts"

archiveChannels-audio()      { "$YTSCRIPTDIR/archivistChannels-audio.sh" "$@"; }
archiveChannels-noComments() { "$YTSCRIPTDIR/archivistChannels-noComments.sh" "$@"; }
archiveChannels-recent()     { "$YTSCRIPTDIR/archivistChannels-recent.sh" "$@"; }
archiveChannels()            { "$YTSCRIPTDIR/archivistChannels.sh" "$@"; }
archivePlaylists-audio()     { "$YTSCRIPTDIR/archivistPlaylists-audio.sh" "$@"; }
archivePlaylists-noComments(){ "$YTSCRIPTDIR/archivistPlaylists-noComments.sh" "$@"; }
archivePlaylists-recent()    { "$YTSCRIPTDIR/archivistPlaylists-recent.sh" "$@"; }
archivePlaylists()           { "$YTSCRIPTDIR/archivistPlaylists.sh" "$@"; }
archiveUnique-audio()        { "$YTSCRIPTDIR/archivistUnique-audio.sh" "$@"; }
archiveUnique-noComments()   { "$YTSCRIPTDIR/archivistUnique-noComments.sh" "$@"; }
archiveUnique-recent()       { "$YTSCRIPTDIR/archivistUnique-recent.sh" "$@"; }
archiveUnique()              { "$YTSCRIPTDIR/archivistUnique.sh" "$@"; }
listenAudio()                { "$YTSCRIPTDIR/audioListen.sh" "$@"; }
watchMobile()                { "$YTSCRIPTDIR/watchOnMobileDevices.sh" "$@"; }
watchPC()                    { "$YTSCRIPTDIR/watchOnPC.sh" "$@"; }
