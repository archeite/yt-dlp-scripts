# yt-dlp Scripts

A collection of shell scripts for [yt-dlp](https://github.com/yt-dlp/yt-dlp).
Scripts are based off [TheFrenchGhosty/TheFrenchGhostys-Ultimate-YouTube-DL-Scripts-Collection](https://github.com/TheFrenchGhosty/TheFrenchGhostys-Ultimate-YouTube-DL-Scripts-Collection).

## Changes

- `docs/` folder removed
- Patched in [PR #90](https://github.com/TheFrenchGhosty/TheFrenchGhostys-Ultimate-YouTube-DL-Scripts-Collection/pull/90) (Reformatted All Scripts)
- `--download-archive`, `-batch-file`, and `... | tee output.log` removed from scripts`

## Features

- [Copylefted libre software](https://github.com/TheFrenchGhosty/TheFrenchGhostys-YouTube-DL-Archivist-Scripts) (GPLv3 licensed)
- Download content in the best possible quality, better than every other software, period.
- Download all kind of content: channels, playlists and individual videos
- Download audio only content, in the best possible quality
- Content separated in two to be easier to archive
- Dedicated scripts to download videos destined to be watched and deleted on a PC
- Dedicated scripts to download videos destined to be watched and deleted on a Mobile device
- Easily expandable for users familiar with yt-dlp / youtube-dl
- [SponsorBlock](https://sponsor.ajay.app/) integration using [mpv_sponsorblock](https://github.com/po5/mpv_sponsorblock) by [@po5](https://github.com/po5)
- [Jellyfin](https://jellyfin.org/) integration using [Jellyfin YouTube Metadata Plugin](https://github.com/ankenyr/jellyfin-youtube-metadata-plugin) by [@ankenyr](https://github.com/ankenyr)
- No Contributor License Agreement
- No Code of Conduct

## Documentation

Basically Everything is basically the same so go to the [original documentation](https://github.com/TheFrenchGhosty/TheFrenchGhostys-Ultimate-YouTube-DL-Scripts-Collection#documentation).

### Alias / Functions

To make your life more easier, you can the functions below to
`.bashrc` or `.zshrc` on Linux (depends on your shell) and on macOS add it to
`.bash_profile`. Make sure to change the `YTSCRIPTDIR` to wherever you put the
scripts in.

Feel free to change the function names to something that suits your needs!

```bash
YTSCRIPTDIR="$HOME/yt-dlp-scripts/scripts"

archiveChannels-audio()      { "$YTSCRIPTDIR/archivistChannels-audio.sh" "$@"; }
archiveChannels-noComments() { "$YTSCRIPTDIR/archivistChannels-noComments.sh" "$@"; }
archiveChannels-recent()     { "$YTSCRIPTDIR/archivistChannels-recent.sh" "$@"; }
archiveChannels()            { "$YTSCRIPTDIR/archivistChannels.sh" "$@"; }
archivePlaylists-audio()     { "$YTSCRIPTDIR/archivistPlaylists-audio.sh" "$@"; }
archivePlaylists-noComments(){ "$YTSCRIPTDIR/archivistPlaylists-noComments.sh" "$@"; }
archivePlaylists-recent()    { "$YTSCRIPTDIR/archivistPlaylists-recent.sh" "$@"; }
archivePlaylists()           { "$YTSCRIPTDIR/archivistPlaylists.sh" "$@"; }
archiveUnique-audio()        { "$YTSCRIPTDIR/archivistUnique-audio.sh" "$@"; }
archiveUnique-noComments()   { "$YTSCRIPTDIR/archivistUnique-noComments.sh" "$@"; }
archiveUnique-recent()       { "$YTSCRIPTDIR/archivistUnique-recent.sh" "$@"; }
archiveUnique()              { "$YTSCRIPTDIR/archivistUnique.sh" "$@"; }
listenAudio()                { "$YTSCRIPTDIR/audioListen.sh" "$@"; }
watchMobile()                { "$YTSCRIPTDIR/watchOnMobileDevices.sh" "$@"; }
watchPC()                    { "$YTSCRIPTDIR/watchOnPC.sh" "$@"; }
```

*(also avaliable at [`functionExample.sh`](./functionExample.sh))*

### Dependencies

- [yt-dlp](https://github.com/yt-dlp/yt-dlp)
- [ffmpeg](https://www.ffmpeg.org/)
- [yt-dlp dependencies](https://github.com/yt-dlp/yt-dlp#dependencies) *Optional*

### Windows Installation

> The Windows version, while it existed, required a lot of work to maintain
> because it needed specific changes compared to the linux version, and was
> never extensively tested. Windows users need to use the scripts in WSL,
> Cygwin, Git Bash, or some other Linux-on-Windows environment that enables
> Bash functionality in Windows.
>
> Installation using WSL (recommended): [Here](https://github.com/TheFrenchGhosty/TheFrenchGhostys-Ultimate-YouTube-DL-Scripts-Collection/blob/master/docs/WSL.md)
>
> Installation using Cygwin, Git Bash or similar: [Here](https://github.com/TheFrenchGhosty/TheFrenchGhostys-Ultimate-YouTube-DL-Scripts-Collection/blob/master/docs/Cygwin-Git-Bash.md)
>
> Downloaded videos will likely exceed Windows' 260-character path length limit
> and not be playable out of the box. You should read more about
> [paths](docs/About-Paths.md) to familiarize yourself with the issue, as well as
> some potential workarounds.
>
> Taken from [TheFrenchGhosty/TheFrenchGhostys-Ultimate-YouTube-DL-Scripts-Collection](https://github.com/TheFrenchGhosty/TheFrenchGhostys-Ultimate-YouTube-DL-Scripts-Collection#installation-windows)

### Other

You can find other documentation at the [original forked repository](https://github.com/TheFrenchGhosty/TheFrenchGhostys-Ultimate-YouTube-DL-Scripts-Collection#documentation).
